<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page session="false" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="zh-CN">
 	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    
      <!-- Bootstrap -->
    <link href="${ctx}/resources/plugin/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
	<script src="${ctx}/resources/plugin/jquery/jquery-1.11.3.min.js"></script>

	<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
	<script src="${ctx}/resources/plugin/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->	
    
<head>
	<title>Home</title>
</head>
<body>
<h1>
	Hello world!  
</h1>

<P>  The time on the server is ${serverTime}. </P>
	<div><a type="button" class="btn btn-primary" href="${ctx}/employee/createEmployee">新增员工</a></div>
	<table class="table table-bordered table-striped ">
      <thead>
        <tr>
          <th>名称</th>
          <th>电话</th>
          <th>邮箱</th>
          <th>是否可用</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
        
        	<c:forEach items="${employees}" var="e">
			<tr>
				<td>${e.emName}</td>
				<td>${e.emPhone}</td>
				<td>${e.emEmail}</td>
				<td>
						 <c:choose>
							    <c:when test="${e.emStatus==1}">
							      	可用
							    </c:when>
							    
							   <c:otherwise>  
							    不可用
							   </c:otherwise>
							  </c:choose>
				
				
				</td>
				<td><a href="${ctx}/employee/update/${e.id}">编辑</a>&nbsp;<a href="${ctx}/employee/delete/${e.id}">删除</a></td>
			</tr>
		</c:forEach>
        
      </tbody>
    </table>
	
	</table>
	
	

</body>
</html>
