<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page session="false" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="zh-CN">
 	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    
      <!-- Bootstrap -->
    <link href="${ctx}/resources/plugin/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="${ctx}/resources/plugin/bootstrap-datetimepicker-0.0.11/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <script src="${ctx}/resources/plugin/bootstrap-datetimepicker-0.0.11/js/bootstrap-datetimepicker.min.js"></script>
      
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
	<script src="${ctx}/resources/plugin/jquery/jquery-1.11.3.min.js"></script>

	<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
	<script src="${ctx}/resources/plugin/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="//cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->	
    
<head>
	<title>Home</title>
</head>
<body>
	<form id="employeeForm" action="${ctx}/employee/createEmployee" method="post" class="form-horizontal">
		<input type="hidden" name="id" value="${e.id}"/>
		<fieldset>
			<legend><small>管理员工</small></legend>
			<div class="control-group">
				<label for="task_title" class="control-label">名称:</label>
				<div class="controls">
					<input type="text" id="emName" name="emName"  value="${e.emName}" class="input-large required" minlength="3"/>
				</div>
			</div>	
			<div class="control-group">
				<label for="task_title" class="control-label">电话:</label>
				<div class="controls">
					<input type="text" id="emPhone" name="emPhone"  value="${e.emPhone}" class="input-large required" />
				</div>
			</div>	
			
			<div class="control-group">
				<label for="task_title" class="control-label">邮箱:</label>
				<div class="controls">
					<input type="text" id="emEmail" name="emEmail"  value="${e.emEmail}" class="input-large required" />
				</div>
			</div>	
			<div class="control-group">
				<label for="task_title" class="control-label">状态:</label>
				<div class="controls">
					<input type="text" id="emStatus" name="emStatus"  value="${e.emStatus}" class="input-large required" />
				</div>
			</div>	
			
			<div class="control-group">
				<label for="task_title" class="control-label">时间:</label>
				<div class="controls">
					<!--input type="text" id="emStatus" name="emStatus"  value="${e.emStatus}" class="input-large required" /-->
					<div id="datetimepicker1" class="input-append date">
					    <input data-format="dd/MM/yyyy hh:mm:ss" type="text"  value="${e.joinDate}" ></input>
					    <span class="add-on">
					      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
					      </i>
					    </span>
					  </div>
				</div>
			</div>	
			
			<div class="control-group">
				<label for="description" class="control-label">备注:</label>
				<div class="controls">
					<textarea id="emRemark" name="emRemark" class="input-large">${e.emRemark}</textarea>
				</div>
			</div>	
			<div class="form-actions">
				<input id="submit_btn" class="btn btn-primary" type="submit" value="提交"/>&nbsp;	
				<input id="cancel_btn" class="btn" type="button" value="返回" onclick="history.back()"/>
			</div>
		</fieldset>
	</form>
	<script>
		$(document).ready(function() {
			//聚焦第一个输入框
			$("#task_title").focus();
			//为inputForm注册validate函数
			$("#inputForm").validate();
		});
	</script>
	<script type="text/javascript">
	  $(function() {
	    $('#datetimepicker1').datetimepicker({
	      language: 'pt-BR'
	    });
	  });
</script>

</body>
</html>
