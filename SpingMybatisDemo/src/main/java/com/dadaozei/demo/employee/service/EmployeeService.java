/**
 * 
 */
package com.dadaozei.demo.employee.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dadaozei.demo.employee.dao.EmployeeDao;
import com.dadaozei.demo.employee.entity.Employee;


/**
 * @author zhaoshuli
 *
 */
@Service
public class EmployeeService {
	
	
	@Autowired
	private EmployeeDao employeeDao;
	
	
	/**
	 * 获得所有员工信息
	 * @return
	 */
	public List<Employee> getAllEmployees(){
		return employeeDao.getAllEmployees();
	}
	
	
	/**
	 * 删除员工信息
	 * @param id
	 */
	public void deleteEmployee(Long id){
		employeeDao.deleteEmployee(id);
	}
	
	
	/**
	 * 新增员工信息
	 * @param employee
	 */
	public void addEmployee(Employee employee){
		employeeDao.addEmployee(employee);
	}
	
	/**
	 * 修改员工信息
	 * @param employee
	 */
	public void updateEmployee(Employee employee){
		employeeDao.updateEmployee(employee);
	}
	
	/**
	 * 查询员工信息
	 * @param id
	 * @return
	 */
	public Employee selectEmployee(Long id){
		return employeeDao.selectEmployee(id);
	}
//	@Override
//	public Page<Employee> getPage(Map<String, Object> searchParams, int pageNo, int pageSize) {
//		return employeeDao.getPage(searchParams, pageNo, pageSize);
//	}

	
}
