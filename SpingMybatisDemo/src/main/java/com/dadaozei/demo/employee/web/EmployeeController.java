/**
 * 
 */
package com.dadaozei.demo.employee.web;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.dadaozei.demo.employee.entity.Employee;
import com.dadaozei.demo.employee.service.EmployeeService;

/**
 * @author zhaoshuli
 *
 */
@Controller
@RequestMapping(value = "/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String listEmployee(Locale locale, Model model) {
		model.addAttribute("employees",employeeService.getAllEmployees());
		return "employee/employeeList";
	}
	
	@RequestMapping(value = "createEmployee", method = RequestMethod.GET)
	public String createEmployee(Locale locale, Model model) {
		model.addAttribute("e", new Employee());
		return "employee/employeeForm";
	}
	
	@RequestMapping(value = "createEmployee", method = RequestMethod.POST)
	public String addEmployee(@ModelAttribute("employeeForm") Employee employee,Locale locale, Model model) {
		
		System.out.println(employee.getEmEmail());
		employee.setJoinDate(System.currentTimeMillis());
		employeeService.addEmployee(employee);
		
		return "redirect:/employee";
	}
	
	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") Long id, Model model) {
		model.addAttribute("e", employeeService.selectEmployee(id));
		return "employee/employeeForm";
	}
	
	@RequestMapping(value = "delete/{id}")
	public String delete(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
		employeeService.deleteEmployee(id);
		return "redirect:/employee";
	}
}
