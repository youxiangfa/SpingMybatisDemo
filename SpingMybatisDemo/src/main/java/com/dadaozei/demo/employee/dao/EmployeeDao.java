/**
 * 
 */
package com.dadaozei.demo.employee.dao;

import java.util.List;

import com.dadaozei.demo.common.persistence.annotation.MyBatisDao;
import com.dadaozei.demo.employee.entity.Employee;

/**
 * @author zhaoshuli
 *
 */
@MyBatisDao
public interface EmployeeDao {

	/**
	 * 获得所有员工信息
	 * @return
	 */
	public List<Employee> getAllEmployees();
	
	/**
	 * 删除员工信息
	 * @param id
	 */
	public void deleteEmployee(Long id);
	
	
	/**
	 * 新增员工信息
	 * @param employee
	 */
	public void addEmployee(Employee employee);
	
	/**
	 * 修改员工信息
	 * @param employee
	 */
	public void updateEmployee(Employee employee);
	
	/**
	 * 查询员工信息
	 * @param id
	 * @return
	 */
	public Employee selectEmployee(Long id);
}
