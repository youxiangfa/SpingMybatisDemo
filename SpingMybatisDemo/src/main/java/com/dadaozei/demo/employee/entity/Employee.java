/**
 * 
 */
package com.dadaozei.demo.employee.entity;

import java.io.Serializable;

/**
 * @author zhaoshuli
 *
 */
public class Employee  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3730144913874854124L;

	/**
	 * 
	 */
	
	private Long id;
	
	/**
	 * 名字
	 */
	private String  emName;
	/**
	 * 电话
	 */
	private String  emPhone;
	/**
	 * 邮箱
	 */
	private String  emEmail;
	/**
	 * 备注
	 */
	private String  emRemark;
	/**
	 * 状态 1:可用 0：不可用
	 */
	private int  emStatus;
	/**
	 * 加入日期
	 */
	private Long  joinDate;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmName() {
		return emName;
	}
	public void setEmName(String emName) {
		this.emName = emName;
	}
	public String getEmPhone() {
		return emPhone;
	}
	public void setEmPhone(String emPhone) {
		this.emPhone = emPhone;
	}
	public String getEmEmail() {
		return emEmail;
	}
	public void setEmEmail(String emEmail) {
		this.emEmail = emEmail;
	}
	public String getEmRemark() {
		return emRemark;
	}
	public void setEmRemark(String emRemark) {
		this.emRemark = emRemark;
	}
	public int getEmStatus() {
		return emStatus;
	}
	public void setEmStatus(int emStatus) {
		this.emStatus = emStatus;
	}
	public Long getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(Long joinDate) {
		this.joinDate = joinDate;
	}
	
	
	
}
